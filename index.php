<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Learning PHP</title>
    <h1>Learning PHP</h1>
  </head>
  <body>
    <?php
      // echo "Hello world" . " - What a beautiful evening!";

      /*
       * Multi line
       * Comment
       */

      echo "<h2>Variables</h2>";
      $a = 5;
      $b = 6;

      if ($a > 4) {
        echo "Variable A is higher than 4<br>";
      } else {
        echo "Variable A is lower than 4<br>";
      }
      echo sprintf("Sum of A and B: %d", $a + $b);

      echo "<h2>Loops</h2>";
      for ($i = 0; $i < 10; $i++) {
        echo $i;
      }

      echo "<br>";

      $i = 0;
      while ($i < 10) {
        echo $i++;
      }

      echo "<br>";

      foreach (range(1,10) as $value) {
        echo $value;
      }
      echo "<br>";

      echo "<h2>Arrays and Maps</h2>";

      $array = ["milk", "eggs", "bread", 1234];
      
      for ($i = 0; $i < sizeof($array); $i++) {
        echo "" . $array[$i] . "<br>";
      }

      $map = ["milk" => 3, "eggs"=> 6];

      foreach ($map as $key => $value) {
        echo $key . ": " . $value . "<br>";
      }

    ?>
  <form action="process.php" method="post">
    <input type="title" name="text" />
    <input type="submit" />
  </form>
  </body>
</html>